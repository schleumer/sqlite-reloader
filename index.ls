sqlite3 = require 'sqlite3' .verbose!
Promise = require 'bluebird'
require! \fs
require! \filewatcher
require! \colors

opts = {
  debounce: 10      # debounce events in non-polling mode by 10ms 
  interval: 1000    # if we need to poll, do it every 1000ms 
  persistent: true  # don't end the process while files are watched 
}



db = new sqlite3.Database ':memory:'
db = Promise.promisify-all db
{ color-it } = require './color-it'

console.log 'running'

run = (sql, delimiter = ';') ->
  sql = sql.replace(/\/\*([\s\S]*?)\*\//mg, '')
  Promise.map do
    sql.split /;/gm .map (.trim!) .filter (.trim!)
    (query) ->
      console.log "Executando:", ("\n" + (color-it query, 'sql') .trim! .split '\n' .map ("  " +) .join '\n'), "\n"
      db.run-async query
    concurrency: 1
  .then ->
    query """SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;"""
      .then (rows) ->
        Promise.map do
          rows
          (row) ->
            Promise.all [
              query "PRAGMA table_info(`#{row.name}`);"
                .then (rows) ->
                  console.log "columns in #{row.name}"
                  rows.for-each (row) ->
                    console.log ("\x1b[38;5;28m" + row.name + "\x1b[0m"), " -> ", ("\x1b[38;5;23m" + row.type + "\x1b[0m")
                  console.log '\n'
                  rows
              query "SELECT name FROM sqlite_master WHERE type = 'index' AND tbl_name = '#{row.name}' ORDER BY name"
                .then (rows) ->
                  console.log "indices in #{row.name}"
                  rows.for-each (row) ->
                    console.log ("\x1b[38;5;28m" + row.name + "\x1b[0m")
                  console.log '\n'
                  rows
            ] .then -> true

      .catch (err) -> throw err
  .catch (err) -> throw err

query = (sql, params = []) ->
  new Promise (resolve, reject) ->
    cb = (err, data) ->
      throw err if err
      resolve data

    db.all sql, params, cb
      
  
running = false
rerun = false
reload = ->
  if running
    console.log 'will rerun'
    rerun := true
    return
  running := true
  db.close!
  db := new sqlite3.Database ':memory:'
  db := Promise.promisify-all db
  file = (fs.read-file-sync 'test.sql').to-string!
  run file .then (x) ->
    running := false
    if rerun
      reload!
      rerun := false
    console.log "tudo ok"

#reload!
#  .then ->
#    query """PRAGMA database_list"""
#      .then (rows) ->
#        console.log rows

reload!

watcher = filewatcher opts

watcher.add './test.sql'

watcher.on 'change', (file, stat) ->
  console.log 'File modified: %s', file
  console.log "Reloading SQLite #{new Date!get-time!}"
  reload!



set-interval (->), 1000