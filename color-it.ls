highlight = require 'highlight.js'
cheerio = require 'cheerio'    

export color-it = (code, lang) ->
  html = (highlight.highlight lang, code).value
  
  $ = cheerio.load("<div id=\"the-wrapper\">#{html}</div>")
  
  elements = []
  ($ '#the-wrapper *').each ->
    let el = ($ this)
      elements.push [el.children!length, el]

  wtf-tag = new Date!get-time!to-string!
  
  # never ever do this at home:
  elements
    .sort (x, y) ->
      if x.0 < y.0
        -1
      else if x.0 > y.0
        1
      else 0
    .reverse!for-each ([..._, el]) ->
      if el.children!length < 1
        el.before("__open_#{wtf-tag}__\x1b[38;5;25m")
        el.after("\x1b[0m__close_#{wtf-tag}__")

  ($ '#the-wrapper').text!
    .trim!
    .replace (new RegExp "__close_#{wtf-tag}__([\\s\\S]*?)__open_#{wtf-tag}__", "mg"), "\x1b[38;5;15m$1\x1b[0m"
    .replace (new RegExp "^([\\s\\S]*?)__open_#{wtf-tag}__", "mg"), "\x1b[38;5;15m$1\x1b[0m"
    .replace (new RegExp "__close_#{wtf-tag}__([\\s\\S]*)", "mg"), "\x1b[38;5;15m$1\x1b[0m"